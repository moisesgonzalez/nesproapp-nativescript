var config = require("../../shared/config");
var frameModule = require("ui/frame");
var fetchModule = require("fetch");
var Observable = require("data/observable").Observable;
var ObservableArray = require("data/observable-array").ObservableArray;
var dialogsModule = require("ui/dialogs");
var http = require("http");

var LoadingIndicator = require("nativescript-loading-indicator").LoadingIndicator;
var loader = new LoadingIndicator();

frameModule.Frame.defaultTransition = { name: "slide" };

function Reporte(info) {
    info = info || {};

    var viewModel = new Observable({});

    /*viewModel.solicitar = function(maquina) {
        
    };*/

    viewModel.estatus = function(comercio) {
    	var options = {
          message: 'Espere un momento...',
          progress: 0.65,
          android: {
            indeterminate: true,
            cancelable: true,
            max: 100,
            progressNumberFormat: "%1d/%2d",
            progressPercentFormat: 0.53,
            progressStyle: 1,
            secondaryProgress: 1    
          }
        };

        loader.show(options);

        console.log("url: " + comercio.comercioURL);

        http.request({ 
            url: comercio.comercioURL + '/incidentes' + "?api_token=" + config.api_token, 
            method: "GET" 
        }).then(function (response) {
		    
		    loader.hide();
		    
		    var estatus = JSON.parse(response.content);

            var navigationEntry = {
	            moduleName: "views/reporte/estatus/estatus",
	            context: {
	            	reporte: viewModel, 
	            	comercio: comercio,
	            	estatus: new ObservableArray(estatus)
	            },
	            animated: true
	        };
	        frameModule.Frame.defaultTransition = { name: "slideLeft" };
	        frameModule.topmost().navigate(navigationEntry);

		}, function (error) {
		    
		    loader.hide();
		    
		    console.log("Error: " + error);

            dialogsModule.alert({
                message: "Por favor verifique su conexión.",
                okButtonText: "Aceptar"
            });

            return Promise.reject();
		});        
    };

    return viewModel;
}

function handleErrors(response) {
    if (!response.ok) {
        console.log(JSON.stringify(response));
        throw Error(response.statusText);
    }
    return response;
}

module.exports = Reporte;