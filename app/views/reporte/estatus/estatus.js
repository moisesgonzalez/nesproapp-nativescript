var frameModule = require("ui/frame");
var dialogsModule = require("ui/dialogs");
var Observable = require("data/observable").Observable;
var ObservableArray = require("data/observable-array").ObservableArray;

var page;
var context;
var pageData;

function pageNavigatedTo(args) {
    page = args.object;
    context = page.navigationContext;

    console.log(context.estatus);
    
    page.bindingContext = context;
}
exports.onPageLoaded = pageNavigatedTo; 

exports.volver = function(){
	frameModule.Frame.defaultTransition = { name: "slideRight" };
	frameModule.topmost().goBack();
}